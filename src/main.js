// === DEFAULT / CUSTOM STYLE ===
// WARNING! always comment out ONE of the two require() calls below.
// 1. use next line to activate CUSTOM STYLE (./src/themes)
// require(`./themes/app.${__THEME}.styl`)
// 2. or, use next line to activate DEFAULT QUASAR STYLE
require(`quasar/dist/quasar.${__THEME}.css`)
    // ==============================

// Uncomment the following lines if you need IE11/Edge support
// require(`quasar/dist/quasar.ie`)
// require(`quasar/dist/quasar.ie.${__THEME}.css`)

import Vue from 'vue'
import VueCordova from 'vue-cordova'
import Quasar from 'quasar'
import * as VueGoogleMaps from 'vue2-google-maps'
import router from './router'
import firebase from 'firebase'
import {
    config
} from './config'

Vue.config.productionTip = false
Vue.use(Quasar) // Install Quasar Framework
Vue.use(VueCordova)
Vue.use(VueGoogleMaps, {
    load: {
        key: 'AIzaSyDLofijGD3310FsWfp0iScEVPQhADH9blk',
        libraries: 'places'
    }
})

//Quasar Styles import
if (__THEME === 'mat') {
    require('quasar-extras/roboto-font')
}
import 'quasar-extras/material-icons'
// import 'quasar-extras/ionicons'
// import 'quasar-extras/fontawesome'
// import 'quasar-extras/animate'

import 'quasar-extras/animate/fadeIn.css'
import 'quasar-extras/animate/fadeOut.css'

// Domain
import User from './domain/user/User'
import UserService from './domain/user/UserService'

Quasar.start(() => {
    /* eslint-disable no-new */
    new Vue({
        el: '#q-app',
        router,
        created() {
            const vm = this

            firebase.initializeApp(config)

            // Initiate User Service
            vm.service = new UserService(firebase.database())

            firebase.auth().onAuthStateChanged((user) => {
                const isUserSet = user => user !== null

                if (isUserSet(user)) {
                    vm.service.update_user(user.uid, user.displayName, user.email)
                    vm.$router.push('/home')
                } else {
                    vm.$router.push('/')
                }
            })
        },
        render: h => h(require('./App').default)
    })
})