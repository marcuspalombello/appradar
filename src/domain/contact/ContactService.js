export default class ContactService {
    constructor(firebase) {
        this._firebase = firebase
    }
    push_permission(user_uid, user_display_name, contact_uid, contact_display_name) {
        this._firebase.ref('geolocation')
            .push({
                // Who is ask permission - Actual User
                primary_user: user_uid,
                primary_name: user_display_name,
                primary_auth: true,
                // Who has been asked permission - Another User
                secondary_user: contact_uid,
                secondary_name: contact_display_name,
                secondary_auth: false
            })
    }
    remove_contact(uid, contact_id) {
        this._firebase.ref('users/' + uid + '/contacts/' + contact_id).remove()
    }
    list_contacts(uid) {
        return this._firebase.ref('users/' + uid + '/contacts')
    }
    push_contact(uid, display_name, email, phone_number) {
        this._firebase.ref('users/' + uid + '/contacts')
            .push({
                display_name: display_name ? display_name : '',
                email: email ? email : '',
                phone_number: phone_number ? phone_number : ''
            })
    }
}