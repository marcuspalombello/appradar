<?php
return [
    'settings' => [
        // Database settings
        'db' => [
            'host' => 'localhost',
            'user' => 'usuario',
            'pass' => 'usuario',
            // 'user' => 'root',
            // 'pass' => '19d31e3b32ff4a3724bdbfbeed9e69ace1d4b6eb40f4c994',
            'dbname' => 'appradar'
        ],
        'displayErrorDetails' => true, // set to false in production
        'addContentLengthHeader' => false, // Allow the web server to send the content-length header

        // Renderer settings
        'renderer' => [
            'template_path' => __DIR__ . '/../templates/',
        ],

        // Monolog settings
        'logger' => [
            'name' => 'slim-app',
            'path' => isset($_ENV['docker']) ? 'php://stdout' : __DIR__ . '/../logs/app.log',
            'level' => \Monolog\Logger::DEBUG,
        ],
        "determineRouteBeforeAppMiddleware" => true,
    ],
];
