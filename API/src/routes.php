<?php

use Slim\Http\Request;
use Slim\Http\Response;

// Routes

//////////////
/* CONTACTS */
//////////////

$app->post('/setContacts', function (Request $request, Response $response, array $args) {

    $data = $request->getParsedBody();
    $return = array();

    if ($data) {
        $new_string = str_replace(' ', '', $data['phone_number']);
        $new_string = str_replace('-', '', $new_string);
        $new_string = trim($new_string);
        $new_string = preg_replace('/[^A-Za-z0-9\-]/', '', $new_string);

        $this->db->insert('contacts', [
            'user_uid' => $data['user_uid'],
            'display_name' => $data['display_name'],
            'email' => $data['email'],
            'phone_number' => $new_string,
        ]);

        $return['status'] = ['error' => 0, 'status' => 'post success'];
    } else {
        $this->logger->info('/setContacts * Could not set contact - Not found $data array - ' . date('d-m-Y'));

        $return['status'] = ['error' => 1, 'status' => 'post error'];
    }

    $response->getBody()->write(json_encode($return));

    return $response->withHeader('Content-type', 'application/json');
});

$app->put('/askContactPermission', function (Request $request, Response $response, array $args) {

    $data = $request->getParsedBody();
    $return = array();

    if ($data) {
        $this->db->update('contacts', [
            'permission_solicitation' => 1
        ], [
            'id' => $data['id']
        ]);

        $return['status'] = ['error' => 0, 'status' => 'put success'];
    } else {
        $this->logger->info('/putUser * Could not put user - Not found $data array - ' . date('d-m-Y'));

        $return['status'] = ['error' => 1, 'status' => 'put error'];
    }

    $response->getBody()->write(json_encode($return));

    return $response->withHeader('Content-type', 'application/json');

});

// NOT USING
$app->get('/askContactPersmission/{uid}/{contact_email}/{contact_phone_number}', function (Request $request, Response $response, array $args) {

    $uid = $args['uid'];
    $contact_email = $args['contact_email'];
    $contact_phone_number = $args['contact_phone_number'];
    $return = array();

    if ($contact_email || $contact_phone_number) {
        // Check if User exists
        $user = $this->db->select('users', '*', [
            'OR' => [
                'email' => $contact_email,
                'phone_number' => $contact_phone_number
            ]
        ]);
        // $contacts = $this->db->select('contacts', '*', [
        //     'user_id' => $user_id
        // ]);

        $return['data'] = $contacts;
        $return['status'] = ['error' => 0, 'status' => 'get success'];
    } else {
        $this->logger->info('/askPermission * Could not select contact - Not found $contact_email OR $contact_phone_number - ' . date('d-m-Y'));

        $return['status'] = ['error' => 1, 'status' => 'get error'];
    }
    
    $response->getBody()->write(json_encode($return));

    return $response->withHeader('Content-type', 'application/json');
});

$app->get('/listContacts/[{user_uid}]', function (Request $request, Response $response, array $args) {

    $user_uid = $args['user_uid'];
    $return = array();

    if ($user_uid) {
        $contacts = $this->db->select('contacts', '*', [
            'user_uid' => $user_uid
        ]);

        $return['data'] = $contacts;
        $return['status'] = ['error' => 0, 'status' => 'list success'];
    } else {
        $this->logger->info('/listContacts * Could not list contacts - Not found $user_id - ' . date('d-m-Y'));

        $return['status'] = ['error' => 1, 'status' => 'list error'];
    }
    
    $response->getBody()->write(json_encode($return));

    return $response->withHeader('Content-type', 'application/json');
});

$app->delete('/deleteContacts/[{contact_id}]', function (Request $request, Response $response, array $args) {

    $contact_id = $args['contact_id'];
    $return = array();

    if ($contact_id) {
        $this->db->delete('contacts', [
            'AND' => [
                'id' => $contact_id
            ]
        ]);

        $return['status'] = ['error' => 0, 'status' => 'delete success'];
    } else {
        $this->logger->info('/deleteContacts * Could not delete contact - Not found $contact_id - ' . date('d-m-Y'));

        $return['status'] = ['error' => 1, 'status' => 'delete error'];
    }
    
    $response->getBody()->write(json_encode($return));

    return $response->withHeader('Content-type', 'application/json');
});

$app->post('/importContacts', function (Request $request, Response $response, array $args) {

    $data = $request->getParsedBody();
    $return = array();

    // $data['contacts'] = $data;
    // $data['user_uid'] = 'asd';

    if ($data) {
        foreach ($data['contacts'] as $c_value) {
            $new_string = str_replace(' ', '', $c_value['phoneNumbers'][0]['value'] ? $c_value['phoneNumbers'][0]['value'] : '');
            $new_string = str_replace('-', '', $new_string);
            $new_string = trim($new_string);
            $new_string = preg_replace('/[^A-Za-z0-9\-]/', '', $new_string);
            
            $this->db->insert('contacts', [
                'user_uid' => $data['user_uid'],
                'display_name' => $c_value['displayName'] ? $c_value['displayName'] : '',
                'display_image' => '',
                'email' => $c_value['emails'][0]['value'] ? $c_value['emails'][0]['value'] : '',
                'phone_number' => $new_string,
                'authorized' => 0
            ]);
        }

        $return['status'] = ['error' => 0, 'status' => 'import success'];
    } else {
        $this->logger->info('/importContacts * Could not import contact - Not found $data array - ' . date('d-m-Y'));

        $return['status'] = ['error' => 1, 'status' => 'import error'];
    }

    $response->getBody()->write(json_encode($return));

    return $response->withHeader('Content-type', 'application/json');
});

///////////
/* USERS */
///////////

$app->post('/setUser', function (Request $request, Response $response, array $args) {

    $data = $request->getParsedBody();
    $return = array();

    if ($data) {
        $user = $this->db->select('users', '*', [
            'uid' => $data['uid']
        ]);

        if (!$user) {
            $this->db->insert('users', [
                'uid' => $data['uid'],
                'display_name' => $data['display_name'],
                'display_image' => $data['display_image'] ? $data['display_image'] : '',
                'email' => $data['email'],
                'phone_number' => $data['phone_number']
            ]);
        }

        $return['status'] = ['error' => 0, 'status' => 'post success'];
    } else {
        $this->logger->info('/setUser * Could not set user - Not found $data array - ' . date('d-m-Y'));

        $return['status'] = ['error' => 1, 'status' => 'post error'];
    }

    $response->getBody()->write(json_encode($return));

    return $response->withHeader('Content-type', 'application/json');
});

$app->get('/getUsers/[{uid}]', function (Request $request, Response $response, array $args) {

    $uid = $args['uid'];
    $return = array();

    if ($uid) {
        $user = $this->db->select('users', [
            'uid',
            'display_name',
            'display_image',
            'email',
            'phone_number',
            'lat',
            'lng'
        ], [
            'uid' => $uid
        ]);

        $return['data'] = $user[0];
        $return['status'] = ['error' => 0, 'status' => 'get success'];
    } else {
        $this->logger->info('/getUsers * Could not select user - Not found $uid - ' . date('d-m-Y'));

        $return['status'] = ['error' => 1, 'status' => 'get error'];
    }
    
    $response->getBody()->write(json_encode($return));

    return $response->withHeader('Content-type', 'application/json');
});

$app->put('/putUser', function (Request $request, Response $response, array $args) {

    $data = $request->getParsedBody();
    $return = array();

    if ($data) {
        $this->db->update('users', [
            // 'display_name' => $data['display_name'],
            // 'email' => $data['email'],
            'phone_number' => $data['phone_number']
        ], [
            'uid' => $data['uid']
        ]);

        $return['status'] = ['error' => 0, 'status' => 'put success'];
    } else {
        $this->logger->info('/putUser * Could not put user - Not found $data array - ' . date('d-m-Y'));

        $return['status'] = ['error' => 1, 'status' => 'put error'];
    }

    $response->getBody()->write(json_encode($return));

    return $response->withHeader('Content-type', 'application/json');

});