<?php
// DIC configuration

use Medoo\Medoo;

$container = $app->getContainer();

$container['db'] = function ($c) {
	$db = $c->get('settings')['db'];
	$database = new Medoo([
	    'database_type' => 'mysql',
	    'database_name' => $db['dbname'],
	    'server' => $db['host'],
	    'username' => $db['user'],
	    'password' => $db['pass']
	]);
	return $database;
};

// view renderer
$container['renderer'] = function ($c) {
    $settings = $c->get('settings')['renderer'];
    return new Slim\Views\PhpRenderer($settings['template_path']);
};

// monolog
$container['logger'] = function ($c) {
    $settings = $c->get('settings')['logger'];
    $logger = new Monolog\Logger($settings['name']);
    $logger->pushProcessor(new Monolog\Processor\UidProcessor());
    $logger->pushHandler(new Monolog\Handler\StreamHandler($settings['path'], $settings['level']));
    return $logger;
};
