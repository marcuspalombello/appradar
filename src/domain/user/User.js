export default class User {

    constructor(display_name = '', display_image = '', email = '', phone_number = '', lat = '', lng = '') {
        this.display_name = display_name
        this.display_image = display_image
        this.email = email
        this.phone_number = phone_number
        this.lat = lat
        this.lng = lng
    }

}