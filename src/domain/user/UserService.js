export default class UserService {
    constructor(firebase) {
        this._firebase = firebase
    }
    get_user(uid) {
        return this._firebase.ref('users/' + uid)
    }
    update_user(uid, display_name, email) {
        this._firebase.ref('users/' + uid)
            .update({
                display_name: display_name ? display_name : '',
                email: email ? email : '',
            })
    }
    update_user_location(uid, lat, lng) {
        this._firebase.ref('users/' + uid)
            .update({
                lat: lat,
                lng: lng
            })
    }
}