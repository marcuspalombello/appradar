export default class Contact {

    constructor(display_name = '', email = '', phone_number = '') {
        this.display_name = display_name
        this.email = email
        this.phone_number = phone_number
    }

}